#!/usr/bin/env python


"""Time Management Tool (TMT) 


ABOUT:

for tracking, summarizing, plotting daily activities using json-formatted data


TODO: 

# def get all activities in day
# def get all activities in week
# def get all activities in month

# def avg of specific task /24h or /work wk or /week

# def plot specific task over duration (absolute or percent)

# def summarize tot tracked on ea. day in dataset

# def grp by week      --> plot %=hrs/wk-hrs
# def grp by work week --> plot %=hrs/wrk-wk-hrs
# def grp by month --> plot % =hrs/mth-hrs

# handle plotby, plotsep args

# refactor
1. get all unique keys
2. put day (dt-obj) as index with ea. column as duration of activity for all unique activities
3. then use to agregate diff columns for plotting purposes



"""

__author__ = 'Logan P. Riley'
__version__ = '0.3.0'

import os
import sys

from datetime import datetime, timedelta

import pandas as pd

import numpy as np

import matplotlib.pyplot as plt
import matplotlib as mpl

import argparse as ap

import json


#### GLOBALS

NOW = datetime.now()
NOW_STR = NOW.strftime('%Y-%m-%d_%H%M%S')

PWD = os.getcwd()
DEF_DATA_SUBDIR = 'data'
DEF_DATA_PTH = os.path.join(PWD, DEF_DATA_SUBDIR)

DEF_SLEEP_HRS = 8 

# assume all time durations will be managed in terms of hours
MAP2H = {'sec' : 3600,
    'min' : 60,
    'hr' : 1,
    'day': .041667,
    'week' : .00595238095,
    'work-week' : .0083333}




def date_str_to_dt(date_str):
    try:
        return datetime.strptime(date_str, '%Y-%m-%d')
    except Exception as e:
        print(e)
        return None

def datetime_str_to_dt(dt_str):
    try:
        return datetime.strptime(dt_str, "%Y-%m-%d %H:%M:%S")
    except Exception as e:
        print(e)
        return None

#### DATA STRUCTURES

class activity(object):
    """ Data object for storing activities
    """
    def __init__(self, name, date, count, period, units):
        self.name = name 
        self.date = date
        self.dt = date_str_to_dt(date) # for pandas
        self.count = int(count)
        self.period = float(period) # duration
        self.units = units
        self.duration = None # init
        self.dur2hr()

    def dur2hr(self):
        """ convert everything to units of hours
        """
        if self.units not in MAP2H:
            raise ValueError(' specified units={} are not understood '.format(self.units))
        else:
            self.duration = self.count*self.period/MAP2H[self.units]

    def __str__(self):
        s = ''
        s += 'activity name     ={}\n'.format(self.name)
        s += 'activity date     ={}\n'.format(self.date)
        s += 'activity count    ={}\n'.format(self.count)
        s += 'activity period   ={}\n'.format(self.period)
        s += 'activity units    ={}\n'.format(self.units)
        s += 'activity duration ={:.2f} [hr]\n'.format(self.duration)
        return s


    def as_dict(self):

        d = {'date': self.date,
            'name' : self.name,
            'count': self.count,
            'period' : self.period,
            'units' : self.units,
            'duration' : self.duration}
            #'date-dt' : self.dt} # calls to df.append(self.as_dict()) will break with this, TODO: make cleaner

        return d



#### FUNCTIONS

def arg_parse():

    print(' > parsing cmd line argument')
    parser = ap.ArgumentParser(description="Time Management Tool")

    # specify source dir(s), else use default
    parser.add_argument("dirs", type=str, nargs='?', default=DEF_DATA_PTH, help='specify one or more directory paths that may contain jsonf-formatted data files\n DEFAULT = DEF_DATA_PTH')

    # specify time period to operate over (default all)
    parser.add_argument('-b', '--begin', type=str, default=None, help='specify start date for processing of form YYYY-MM-DD')
    
    parser.add_argument('-e', '--end', type=str, default=None, help='specify end data for processing of form YYYY-MM-DD')

    # get stats  (default)
        # nr uniq activities
        # uniq activity names
        # start date
        # end date
        # percent of dataset within specify start/end date
        # highest activity by tota duration
        # lowest activity by total duration
        # total time registered, percent of total  tracked
        # 
    parser.add_argument('-s', '--stats', action='store_true', help='print out default stats for range')

    parser.add_argument('--sleep', type=int, default=None, help='specify number of sleep hours per day expected, used for normalization')

    # get stats on specific activity->key
        # mean, median per mth, wk, work-wk, day-of-week, day
    parser.add_argument('-i', '--inspect', metavar='statvars', type=str, nargs='*', default=None, help='specify particular activities for which you want statistics')

    # plot all activities (default)
    parser.add_argument('-p', '--plot', metavar='plotvars', type=str, nargs='*', default='all', help='specify particular activities to plot')
    
    parser.add_argument('--plotby', type=str, choices=['day', 'week', 'month'], default='day', help='specify how to group data for plotting, valid options are day (default), week, month' )

    ## TODO: plot individually or group
    parser.add_argument('--plotsep', action='store_true', help='toggle if each activity should receive a separate plot')

    parser.add_argument('--plotas', type=str, choices=['line', 'bar', 'stack', 'hist'], default='line', help='specify plot type (only relevant if --plot option used)')

    args = parser.parse_args()

    return vars(args)





def find_all_json_in_dir(dir_pth):
    """ find candidate json files in specified directory path (dir_pth)
    """
    f_pths = []

    for root, _, files in os.walk(dir_pth):
        for f in files:
            pth = os.path.join(root, f) # get full path
            f_pths.append(pth)

    return f_pths


def looks_json(fn):
    """ first coarse filter for checking input data file
    """
    if fn.lower().endswith('json'):
        return True
    else:
        return False


def load_json(fn):
    """ attempt to load json from file and return dict obj
    """

    data = None

    if os.path.isfile(fn):

        try:
            with open(fn, 'r') as fp:
                data = json.load(fp)
            print(' ... ... ... loaded JSON file : {}'.format(fn))
        except Exception as e:
            print(' ERROR : could not decode json, recieved error\n {}'.format(e))
            print(' ... ... .... skipping file : {}'.format(fn))

    return data


def find_candidate_json(dirs_lst):
    """ search all dirs in dirs_lst for potential json data files
        and return as list
    """    
    print(' ... searching for candidate json data')
    json_lst = []
    j = 0
    for i, d in enumerate(dirs_lst):
        print(' ... ... in dir #{} : {}'.format(i+1, d))
        file_cands = find_all_json_in_dir(d)

        for f in file_cands:
            if looks_json(f):       # check if ending in json
                j += 1
                print(' ... ... ... found file : #{} {}'.format(j, f))
                json_lst.append(f)

    return json_lst


def valid_date(date_str):
    """ use datetime to verify if input string can be made into datetime object
    """
    MIN_LEN = 4 + 1 + 2 + 1 + 2 # 'YYYY-MM-DD'

    if len(date_str) < MIN_LEN:
        print(' WARNING: invalid date string len({}) != {}'.format(date_str, MIN_LEN))
        return False

    try:
        _ = date_str_to_dt(date_str)
        return True
    except Exception as e:
        print(' WARNING: unable to convert to datetime object\n {}'.format(e))
        return False
        
    


def load_all_data(fn_lst):
    """ for all candidate data files, load data
    """
    print(' ... loading json data')
    data_lst = []

    for fn in fn_lst:
        data = load_json(fn)

        if data is not None:
            data_lst.append(data)

    if len(data_lst) == 0:
        print(' ... ... WARNING : did not load any data')
        return None

    return data_lst
        

def json_to_act(date, day_d, verbose=False):
    """ take dict obj for activites on given date and convert to activity() date structure
    """
    acts = []

    for i, k in enumerate(day_d): 
        if verbose:
            print(' ... ... ... {} activity : {}'.format(i+1, k))
        # the dict key is the activity name
        # the dict[key] contains
        # time = [count, duration, units]
        time = day_d[k]
        nr = len(time)
        if nr != 3:
            str_ = ' ERROR number of items in activity={} time definition {} !={} required'.format(k, nr, 3)
            raise ValueError(str_)

        acts.append(activity(k, date, time[0], time[1], time[2]))


    return acts

    
def get_uniq_acts(act_lst):
    """ take list of activity objects and populate unique set of activity names
    """
    act_names = set()

    print(' ... ... getting unique activity names')
    for a in act_lst:
        #print(type(a))
        name = a.name
        act_names.add(name)

    nr_names = len(act_names)

    print(' ... ... ... got {} unique activity names'.format(nr_names))

    return act_names


def verify_data(data_lst, verbose=False):
    """
    inspect list of json data objs and return unified list of activity data objects, uniq list of activity names
    """
    # for ea. data key verify match to YYYY-MM-DD
    # merge into single data structure 

    print(' ... verifying data')

    # get all activities
    act_lst = [] # for data objects
    act_names = set()

    a = 0
    # iterate over data objs from sep files
    for d_obj in data_lst:
        # get each date entry from single file
        for date in d_obj:
            # key is date
            if valid_date(date):
                a += 1
                print(' ... ... {:>6d} found date : {}'.format(a, date))

                day_d = d_obj[date]
                acts = json_to_act(date, day_d, verbose=verbose)
                act_lst += acts

            else:
                print(' ... ... WARNING : invalid date string in json, problematic key is {}'.format(date))
        

       # print(d_obj)

    if len(act_lst) < 1:
        print(' ... ... WARNING : no activities found in files')
        return None, None

    # get final list of unique names
    act_names = get_uniq_acts(act_lst)

    return act_lst, act_names


def pd_set_dt(df):
    df['date-dt'] = pd.to_datetime(df['date'])

def init_db(act_lst):
    """ build pandas data frame for easier manipulation
    """

    print(' ... building pandas dataframe')
    # init pd dataframe to store
    names = []
    dates = []
    counts = []
    periods = []
    units = []
    durations = []

    for a in act_lst:
        names.append(a.name)
        dates.append(a.date)
        counts.append(a.count)
        periods.append(a.period)
        units.append(a.units)
        durations.append(a.duration)

    df = pd.DataFrame({'date' : dates,
                     'name' : names,
                    'count' : counts,
                    'period' : periods,
                    "unit": units,
                'duration' : durations})

    pd_set_dt(df)
    #df['date-dt'] = pd.to_datetime(df['date']) # convert str to datetime object
    return df



def get_act_dts(act_lst):
    """ return datetime attr from activity object
    """
    return [a.dt for a in act_lst]

def get_lastest_date(act_lst):
    """ for list of activities objects, retun last date in data
    """
    #dts = [ a.dt for a in act_lst ]
    return max(get_act_dts(act_lst))

def get_earliest_date(act_lst):
    """ for list of activities objects, return first date in data
    """
    #dts = [ a.dt for a in act_lst ]
    return min(get_act_dts(act_lst))   



def pd_get_nr_acts(df):
    return len(df.index)

def pd_btwn_dates(df, begin_dt, end_dt, dt_key='date-dt'):
    """ return dataframe subset between datetime objects
    """
    mask = (df[dt_key] >= begin_dt) & (df[dt_key] <= end_dt)
    return df.loc[mask]


def pd_grp_act_by_days(df):
    return None

def pd_grp_act_by_week(df):
    return None

def pd_grp_act_by_workweek(df):
    return None

def pd_grp_act_by_month(df):
    return None


def pd_get_max_on_row(df, var):
    max_val = df[var].max()

    # must use index to get indices that match, then pull values
    return df[df[var] == max_val ].index.values[0], max_val

def pd_get_min_on_row(df ,var):
    min_val = df[var].min()

    return df[df[var] == min_val ].index.values[0], min_val


def pd_prt_df_by_key(df, key,hdr=None, unit='%', scl=100, skip=None):
    """ iterate over dataframe and print column=key

    skip row with id skip
    """
    if hdr is not None:
        print(hdr)

    for k in range(len(df.index)):
        lbl = df[key].index.values[k]
        val = df[key][k]

        if lbl != skip:
            print(' ... ... ... {:>25s} {:>12.3f} [{}]'.format(lbl, val*scl, unit))


def pd_get_duration(df, key='date-dt', off=0.):
    """ find difference between max and min time in dataframe

    TODO, handle last day + 24 since last day starts at midnight
    """
    return df[key].max() - df[key].min() + timedelta(hours=off)

def dt_to_hrs(dt):
    return dt.total_seconds()/3600.


def gen_global_stats(df, st_date=None, ed_date=None):
    """ Compute general stats on data set


    """
    # init output var
    stats = {}
        
    print(' ... doing basic stats processing')

    first_date = df['date-dt'].min()
    last_date  = df['date-dt'].max()

    stats['first_date'] = first_date
    stats['last_date'] = last_date

    print(' ... ... first date : {}'.format(first_date))
    print(' ... ... last  date : {}'.format(last_date))


    uniq_acts = pd_get_uniq(df, 'name')
    nr_uniq = len(uniq_acts)

    stats['uniq_acts'] = uniq_acts
    stats['nr_uniq_acts'] = nr_uniq
    
    print(' ... ... unique activities:')
    for i, ua in enumerate(uniq_acts):
        print(' ... ... ... {:>10d} {}'.format(i+1, ua))

    nr_acts_full = pd_get_nr_acts(df)

    stats['nr_full'] = nr_acts_full
    print(' ... ... total activities : {}'.format(nr_acts_full))


    # slice between range
    print(' ... ... preparing to slice by date')
    time_ = ' 00:00:00'
    if st_date is not None:
        st_dt = datetime_str_to_dt(st_date + time_)
    
        if st_dt < first_date:
            print(' ... ... ... WARNING : specified start date is earlier than dataset')
            print(' ... ... ... WARNING : using start date in data')

            st_dt = first_date
    else:
        print(' ... ... ... WARNING : using start date in data')
        st_dt = first_date

    if ed_date is not None:
        ed_dt = datetime_str_to_dt(ed_date + time_)

        if ed_dt > last_date:
            print(' ... ... ... WARNING : specified end date is later than dataset')
            print(' ... ... ... WARNING : using end date in data')
            ed_dt = last_date
    else:
        print(' ... ... ... WARNING : using end date in data')
        ed_dt = last_date
    
    df_sub = pd_btwn_dates(df, st_dt, ed_dt)

    nr_acts_sub = pd_get_nr_acts(df_sub)
    stats['nr_sub'] = nr_acts_sub
    print(' ... ... total activities between specified dates {} {} : {}'.format(st_dt,ed_dt,nr_acts_sub))


    dur_sub = pd_get_duration(df_sub, 'date-dt', off=24) # use 24 offset to encapsulate full day
    dur_sub_hrs = dt_to_hrs(dur_sub)
    dur_sub_days = dur_sub_hrs/24.
    dur_sub_mths = dur_sub_days/30.
    dur_sub_workdays = 5*4*dur_sub_mths
    dur_sub_wkdy_hrs = dur_sub_workdays*8

    print(' ... ... total time in slice {:.2f} [hr]'.format(dur_sub_hrs))
  
    df_ut = df_sub[df_sub['name'] == 'untracked']
    untracked_time = df_ut['duration'].agg('sum')
    tracked_time = dur_sub_hrs - untracked_time

    stats['tracked-time'] = tracked_time
    stats['untracked-time'] = untracked_time

    perc_tracked = tracked_time/dur_sub_hrs
    stats['perc-tracked'] = perc_tracked

    df_s_byact = df_sub.groupby('name').agg('sum')
    pd_prt_df_by_key(df_s_byact, 'duration', unit='hr',scl=1)    


    df_s_byact['perc-overall'] = df_s_byact['duration']/dur_sub_hrs
    print(' ... ... percent of total time')
    pd_prt_df_by_key(df_s_byact, 'perc-overall')

   
    df_s_byact['perc-tracked'] = df_s_byact['duration']/tracked_time
    print(' ... ... percent of tracked time')
    pd_prt_df_by_key(df_s_byact, 'perc-tracked', skip='untracked')




    # TODO: percent of work time



    df_slp = df_sub[df_sub['name'] == 'sleep']
    sleep_time = df_slp['duration'].agg('sum')
    wake_time = dur_sub_hrs - sleep_time
    perc_wake = tracked_time/wake_time
    stats['perc-wake'] = perc_wake

    print(' ... ... percent of total time tracked {:.2f} [%]'.format(perc_tracked*100))
    print(' ... ... percent of waking time tracked {:.2f} [%]'.format(perc_wake*100))
    

    # get nr occurences of each activity
    print(' ... ... activity counts')
    counts = pd_get_counts(df_sub, 'name') #df_sub['name'].value_counts()

    for k in counts.index:
        print(' ... ... ... activity {:>24s} count {:>6d}'.format(k, counts[k]))

    act_max_cnt = counts.max()
    act_min_cnt = counts.min()
    act_max = counts[counts == act_max_cnt].index.values
    act_min = counts[counts == act_min_cnt].index.values

    print(' ... ... ... highest occurence for activity {} at {} counts:'.format(act_max, act_max_cnt))
    print(' ... ... ... lowest  occurence for activity {} at {} counts:'.format(act_min, act_min_cnt))


    return stats, df_sub

def pd_get_counts(df, key):
    return df[key].value_counts()

def pd_get_uniq(df, key):
    """ return unique values for given column
    """
    return [v for v in df[key].unique()]


def get_untracked(tracked):
    if tracked <= 24:
        return 24 - tracked
    else:
        return 0

def get_tracked(untracked):
    return 24 - untracked
    

def get_untracked_hrs(df):
    """ for each date, compute untracked hours = 24 - sum(tracked)
    """
    print(' ... computing untracked hours')
    df2 = df.copy() # TODO: remove?

    tmp = df.groupby('date').agg('sum')
    date_t = tmp.index.values # because I'm grouped on these
    durs_t = tmp['duration']

    t_tot = 0

    #print('...')
    for d, t in zip(date_t, durs_t):
        if t > 24:
            print(' ... ... WARNING : date {} accounts for more than 24 hrs : {}'.format(d,t))
            
        u_t = get_untracked(t)
        t_tot += u_t

        act = activity('untracked', d, 1, u_t, 'hr' )
        df2 = df2.append(act.as_dict(), ignore_index=True)

    print(' ... ... total untracked {:.1f} [hr]'.format(t_tot))
    pd_set_dt(df2) # set datetime object from date
    act_names = pd_get_uniq(df2, 'name')

    return df2, act_names




def get_sleep_hrs(df, sleep_hrs=None):
    """ for data set, set number of sleep hours in day using sleep_hrs
    """
    print(' ... setting sleep hrs')

    if sleep_hrs is not None:
        df2 = df.copy()
        uniq_dates = pd_get_uniq(df, 'date') 

        nr_uniq = len(uniq_dates)

        print(' ... ... got {} unique dates'.format(nr_uniq))

        j = 0
        for date in uniq_dates:
            tmp = df[ df['date'] == date]

            # don't overwrite if already in data
            if 'sleep' in tmp['name'].values:
                print(' ... ... skipping date : {}'.format(date))

            else:
                j += 1
                ent = activity('sleep', date, 1, sleep_hrs, 'hr')
                df2 = df2.append(ent.as_dict(), ignore_index=True)

        pd_set_dt(df2) # need to convert date to datetime obj for new entry

    else:
        print(' ... ... WARNING : sleep_hrs is None, skipping')
        df2 = df
        
    print(' ... ... set sleep hours for  {:d} dates'.format(j))
    act_lst = pd_get_uniq(df2, 'name')
    
    return df2, act_lst



def detailed_stats(df, act_lst):
    """ process stats on named activities
    """
    print(' ... doing detailed stats on specific activities')

    if act_lst is None:
        return None

    df_det = df.copy()


    df_det.index = df['date-dt'] 
    df_det.groupby(pd.Grouper(freq='M'))
    print(df_det)


    #print(act_lst)
    # for ea. activity name
    for a in act_lst: 
        print(' ... ... processing for activity {}'.format(a))
        pass


        # median time record/day
        # avg time record/day

        # avg / total time
        # avg / wake time
        # avg / work day
        # avg / week
        # avg / work week
        # avg / month

        # day of highest duration : find max, get day of (date)
        # day of lowest  :

        # date of highest duration
        # most probable day of week to be highest duration



def plot(df, plt_vars, plot_type='line', savefig=True, fig_sz=[8,4], fig_dpi=200, fig_ext='png', fn='tmt', ncol=4):
    """ plotter function for dataframe df considering only plotting variables plt_vars
    """
    print(' ... plotting ')

    # TODO: handled stacked barplot

    fig = plt.figure(figsize=fig_sz, dpi=fig_dpi)
    ax = fig.add_subplot(111)

    nv = len(plt_vars)
    v_txt=','.join(plt_vars)

    print(' ... ... got plot variables : {}'.format(v_txt))



    # TODO: org by week, month as required (plotby)

    interval = 'day'

    tmp = df[df['name'].isin(plt_vars)]

    #print(tmp.head())
    
    if plot_type == 'line':
        for v in plt_vars:
            tmp2 = tmp[ tmp['name'] == v ]   
            tmp2.plot(x='date-dt', y='duration', ax=ax, label=v, marker='o', markersize=2.5, kind=plot_type)
    
    elif plot_type == 'bar':
        #tmp[['name', 'date-dt', 'duration']].plot(x='date-dt', y=plt_vars, ax=ax, kind=plot_type)
        pass
        #for v in plt_vars:
        #    tmp2 = tmp[ tmp['name'] == v ]   
        #    tmp2.plot(x='date-dt', y='duration', ax=ax, label=v, kind=plot_type)

        #tmp.plot(x='name', y='duration', ax=ax, kind=plot_type)
    
    else:
        raise Warning(' ... other plot_type != line not yet fully implemented')
        return None

    ax.set_xlabel('date')
    y_str = 'duration [hr]/{}'.format(interval)
    ax.set_ylabel(y_str)

    # set leg loc


    ax.legend(ncol=ncol)
    fig.tight_layout()

    fn_ = fn + NOW_STR + '.' + fig_ext
    print(' ... ... saving to {}'.format(fn_))
    fig.savefig(fn_, dpi=fig_dpi)

    F = [fig, ax]
    return F
    

###

def prt_hdr():
    print('-'*60)
    print(' /// Time Management Tool ///')
    print('-'*60)

def prt_ftr():
    print('-'*60)
    print('*** COMPLETE ***')
    print('-'*60)

#### MAIN

def tmt(dirs_lst, \
    i_gen_stats=True, begin_date=None, end_date=None, \
    ins_vars=None, plot_vars=None, plot_type=None, \
    sleep_hrs=None, **kwargs):
    """ Main utility

    dirs_lst : list of directories to search for relevant json data

    i_gen_stats : bool
    begin : str of form YYYY-MM-DD for defining starting date
    end : str of form YYYY-MMD-DD for defining end date
    
    ins_vars : list
        a list of activities to inspect

    plot_vars : list
        a list of activities to plot

    


    """
    print(' > processing data')
    # sanity check is list
    if type(dirs_lst) != list:
        dirs_lst = [dirs_lst]
    else:
        pass


    # return list of candidate files to load data from
    fc_lst = find_candidate_json(dirs_lst)

    # find all json looking file in
    raw_data = load_all_data(fc_lst)

    # verify data and massage into 
    verified_data, act_names = verify_data(raw_data)
    act_names = list(act_names)

    # DEBUG
    #print(act_names)

    if verified_data is None:
        print(' WARNING : no valid activities found in presented files')
        return None

    # to pandas for manip
    df_raw = init_db(verified_data)


    # fill in sleep hours, use for gen stats and activity stats

    if sleep_hrs is None:
        sleep_hrs=DEF_SLEEP_HRS

    df, act_names_  = get_sleep_hrs(df_raw, sleep_hrs=sleep_hrs)


    # fill in untracked hours, use for gen stats and activity stats
    df, act_names_ = get_untracked_hrs(df)
    
    
    # stats
    if i_gen_stats and df is not None: # TODO: can df ever be None?
        stats_, df_sub = gen_global_stats(df, st_date=begin_date, ed_date=end_date)
    else:
        df_sub = df
        stats_ = None

    # detailed activity stats
    if ins_vars is not None:
        # TODO: catch if ins_vars = 'all'
        nr_ins = len(ins_vars)

        if nr_ins > 0:
            if nr_ins == 1:
                if ins_vars[0] == 'all':
                    ins_vars = act_names_
                else:
                    ins_vars = [ins_vars] # ensure we have a list
            _ = detailed_stats(df_sub, ins_vars)
            
        else:
            print(' ... WARNING : ins_vars list is empty, cannot process detailed stats')


    # plot
    if plot_vars is not None:
        # TODO: catch if plot_vars = 'all'
        nr_plt = len(plot_vars)

        if nr_plt > 0:
            if nr_plt == 1:
                if plot_vars[0] == 'all':
                    plot_vars = act_names_
            # TODO: call
            # pass plotby, plotsep

            if plot_type is None:
                plot_type = 'line'


            F = plot(df_sub, plot_vars, plot_type=plot_type)
            
        else:
            print(' ... WARNING : plt_vars list is empty, cannot plot data')

 
    return df , stats_




if __name__ == '__main__':
    prt_hdr()
    args = arg_parse() 
    dirs_lst = args['dirs']   

    #print(args['sleep'])

    if args['sleep'] is None:
        sleep_hrs = 8
    else:
        sleep_hrs = args['sleep']
    
    i_gen_stats = args['stats'] 
    
    begin = args['begin']
    end = args['end'] 

    ins_lst = args['inspect']

    plt_type = args['plotas']


    # TODO: handle if provided comma separated string
    ## if ',' in inp, out = inp.split(',')
    plt_lst = list(args['plot'])
    #print(args.keys())
    
    df, stats = tmt(dirs_lst, i_gen_stats=i_gen_stats, begin_date=begin, end_date=end, \
        sleep_hrs=sleep_hrs, ins_vars=ins_lst, plot_vars=plt_lst, plot_type=plt_type, **args)
    prt_ftr()




