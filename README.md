# Time Management Tool


## About

* a tool for tracking, computing statistics on, and plotting one's time
* reads JSON-formatted files containing one or more dates

## License

* see LICENSE

## Requirements

* see requirements.txt

## Example

* sample.json

```{ "2025-01-25" : { "email" : [ count <INT>, interval <INT>/<FLOAT>, units <STR> e.g. "min"],
  	       	   "exercise" : [2, 30, "min"]
		 }
}```



